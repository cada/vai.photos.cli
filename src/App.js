import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import './App.css';
import Footer from './layout/Footer'

import UploadButton from './components/UploadButton';
import ImagesContainer from './components/ImagesContainer';
import ImagePage from './components/ImagePage';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <section className="section">
          <div className="container is-fluid">
            <h1 className="title">
              <Link to="/"><small className="vai">vai.</small>Photos</Link>
              <UploadButton />
            </h1>
          </div>

          <div className="container is-fluid">
            <Route exact path="/" component={ImagesContainer} />
            <Route exact path={'/images/:imageId'} component={ImagePage} />
          </div>

          <Footer />
        </section>
      </BrowserRouter>
    );
  }
}

export default App;
