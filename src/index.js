import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const _baseURL = (process.env.NODE_ENV === 'production') ? 'https://vai-photos-api.herokuapp.com/v1/' : 'http://localhost:3001/v1/';
axios.defaults.baseURL = _baseURL;
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
