import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { withRouter } from 'react-router-dom';
import axios from 'axios';

class UploadButton extends Component {
  constructor() {
    super();

    this.openFileDialog = this.openFileDialog.bind(this)
    this.upload = this.upload.bind(this);
  }
  
  openFileDialog () {
    var fileInputDom = ReactDOM.findDOMNode(this.input)
    fileInputDom.click()
  }

  upload(e) {
    e.preventDefault();
    const _history = this.props.history;
    const file = e.target.files[0];
    const formData = new FormData()
    formData.append('archive[image]', file)

    axios
      .post(`archives.json`, formData)
      .then(function(response){
        const _id = response.data.id
        _history.push(`/images/${_id}`, { type: 'success', message: 'Image created!', editMode: true });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="field is-pulled-right">
        <input type="file" name="image" ref={rel => this.input = rel} className="is-hidden" onChange={this.upload} />
        <button className="button is-primary" onClick={this.openFileDialog}>Upload</button>
      </div>
		);
  }
}

export default withRouter(UploadButton);