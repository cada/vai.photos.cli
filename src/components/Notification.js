import React from 'react';

const Notification = ({ type, message }) =>
  <div className={ `notification is-${type}` }>
    <button className="delete"></button>
    {message} <a href="/">Back</a>
  </div>

export default Notification