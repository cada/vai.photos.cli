import React, { Component } from 'react'
import { Link } from "react-router-dom";

class ImageCard extends Component {

	render() {
		return(
      <div className="column is-one-fifth">
        <div className="card">
          <div className="card-image">
            <Link to={`/images/${this.props.image.id}`}>
              <figure className="image is-4by3 has-background-light">
                <img src={ this.props.image.file_url } alt='' />
              </figure>
            </Link>
          </div>
        </div>
      </div>
		)
	}
}

export default ImageCard