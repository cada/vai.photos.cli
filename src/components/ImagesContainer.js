import React, { Component } from 'react'
import axios from 'axios';

import Notification from './Notification'
import ImageCard from './ImageCard'

class ImagesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      images: [],
      editingImageId: null,
      notification: this.props.history.location.state,
      showNotification: false
    }

    this.notify.bind(this);
  }

  componentDidMount() {
    this.notify()
    axios
      .get('archives.json')
      .then(response => {
        this.setState({ images: response.data })
      })
      .catch(error => console.log(error))
  }

  notify = ()=> {
    if(this.state.notification){
      let notification = this.state.notification;
      this.setState({ showNotification: <Notification type={notification.type} message={notification.message} /> })
    }
  }

  render() {
    const showNotification = this.state.showNotification;

    return (
      <section>
        { showNotification }
        <div className="columns is-multiline">
          { this.state.images.map((image) => {
            return (<ImageCard image={image} key={image.id} />)
          })}
        </div>
      </section>
    )
  }
}

export default ImagesContainer