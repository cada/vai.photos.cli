import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import Notification from './Notification';
import ImageForm from './ImageForm'

const Image = ({ image, destroyImage, editMode }) =>
  <div className="content my-1">
    <h3 className="is-size-4">{ image.title }</h3>
    <blockquote>
      { image.description }<br />
    </blockquote>
    <p>
      by { image.author || 'Anonimous' } @ <time dateTime={ image.created_at }>{ image.created_at }</time><br />
    </p>
    <pre>
      <code className="language-json" data-lang="json">
        { JSON.stringify(image.metadata) }
      </code>
    </pre>
    <br />
    <button onClick={ destroyImage } className="button is-danger is-outlined is-small is-pulled-left">Delete</button>
    <button onClick={ editMode } className="button is-link is-outlined is-small is-pulled-right">Edit Image</button>
  </div>

class ImagePage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.imageId,
      image: {},
      editingMode: false,
      history: this.props.history.location.state,
      showNotification: false
    }

  }

  componentDidMount() {
    if (this.state.history){
      this.notify(this.state.history.type, this.state.history.message)
    }

    axios
      .get(`archives/${this.state.id}.json`)
      .then(response => {
        this.setState({ image: response.data })
      })
      .catch(error => {
        console.log(error);
        console.log(error.response.data);
        this.notify('danger', error.response.data.message)
      });
  }

  destroy = () => {
    const id = this.state.id;
    axios
      .delete(`archives/${id}.json`)
      .then(response => {
        this.props.history.push("/", { type: 'warning', message: 'Boom' });
      })
      .catch(error => console.log(error))
  }

  notify = (type, message)=> {
    this.setState({ showNotification: <Notification type={type} message={message} /> })
  }

  enableEditingMode = ()=> {
    this.setState({ editingMode: !this.state.editingMode })
  }

  updateImageState = (data)=> {
    this.setState({ image: data })
    this.setState({ showNotification: <Notification type='primary' message='Image updated!' /> })
  }

  render(){
    const isEditingModeEnabled = this.state.editingMode
    const showNotification = this.state.showNotification

    return (
      <section>
        { showNotification }
        <div className="columns">
          <div className="column is-half">
            <figure className="image">
              <img src={ this.state.image.file_url } alt={ this.state.image.title } className="rounded" />
            </figure>
          </div>

          <div className="column is-half">
            { isEditingModeEnabled 
              ? <ImageForm image={ this.state.image } key={ this.state.image.id } editMode={this.enableEditingMode} onImageUpdate={this.updateImageState} />
              : <Image image={ this.state.image } key={ this.state.image.id } editMode={this.enableEditingMode} destroyImage={this.destroy} />
            }
          </div>
        </div>
      </section>)
  }
}

export default withRouter(ImagePage);