import React, { Component } from 'react';
import axios from 'axios';

class ImageForm extends Component {
  constructor(props) {
    super(props)

    this._id = this.props.image.id;

    this.state = {
      image: this.props.image
    }

    this.handleInput.bind(this)
  }

  handleInput = (e) => {
    const _value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    const image = this.state.image;
    const updatedImage = {
      ...image,
      [e.target.name]: _value
    }
    this.setState({ image: updatedImage })
  }

  update = (e) => {
    e.preventDefault();
    let data = this.state.image;
    axios
      .put(`archives/${this._id}.json`, { archive: data })
      .then(response => {
        this.props.editMode();
        this.props.onImageUpdate(response.data);
      })
      .catch(error => console.log(error))
  }

  render() {
    return(
      <div className="content my-1">
        <h3 className="is-size-4">Editing</h3>
        <form onSubmit={this.update}>
          <div className="field">
            <label className="label">Title</label>
            <input className='input' type="text"
              name="title" placeholder='Enter a Title'
              value={this.state.image.title || ''} onChange={this.handleInput} />
          </div>

          <div className="field">
            <label className="label">Author</label>
            <input className='input' type="text"
              name="author" placeholder='Author'
              value={this.state.image.author || ''} onChange={this.handleInput} />
          </div>

          <div className="field">
            <label className="label">Description</label>
            <textarea className='textarea' name="description"
              placeholder='Description for this image' rows="3"
              value={this.state.image.description || ''} onChange={this.handleInput}>
            </textarea>
          </div>

          <div className="field">
            <label>
              <input className="checkbox"
                type="checkbox"
                checked={this.state.image.published}
                name="published"
                onChange={this.handleInput} /> Published?
            </label>
          </div>

        <br />

        <button className="button" onClick={ this.props.editMode }>Cancel</button>
        <input type="submit" value="Submit" className="button is-info is-outlined is-pulled-right" />
      </form>
    </div>
   )
 }
}


export default ImageForm;